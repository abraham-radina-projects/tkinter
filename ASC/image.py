from tkinter import *

root = Tk()
label = Label(text="Hello")

# Retrieve a Label's text
text = label["text"]

# Set new text for the label
label["text"] = "Good bye"
root.mainloop()