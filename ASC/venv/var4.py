from tkinter import ttk
from tkinter import *
from tkinter.ttk import Combobox

myWindow = Tk()
myWindow.title("ASC")
myWindow.configure(background="peach puff")
frame1=Frame(height = 180,width = 1000,bg="peach puff")
frame1.pack()
l2 = Label(frame1, text="Reprezentarea si evaluarea functiilor logice",bg="coral1",fg="brown4",font="Helvetica 18 underline italic",height=1)
l2.pack()

l3 = Label(frame1, text="Alegeti numerele in baza 2", fg="coral", font="Times")
l3.pack()

lcombo= Label(frame1, bg="peach puff",height=1)
lcombo.pack()
v=["0","1"]
combo1=Combobox(frame1,values=v,width=15)
combo1.pack()
combo1.set("Primul numar")
combo1.pack()
combo2=Combobox(frame1,values=v,width=15)
combo2.set("Al doilea numar")
combo2.pack()

def AND_GATE():
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 =="1" and value2 =="1":
        print("1")
    else:
        print("0")

def NAND():
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == "1" and value2 == "1":
        print("0")
    else:
        print("1")

def OR():
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == "1":
        print("1")
    elif value2== "1":
        print("1")
    else:
        print("0")

def NOR():
    value1 = combo1.get()
    value2 = combo2.get()
    if value1=="0" and value2=="0":
        print("1")
    else:
        print("0")

def XOR ():
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 != value2:
        print("1")
    else:
        print("0")

def NXOR():
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == value2:
        print("1")
    else:
        print("0")

def NOT():
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == "0":
        val1="1"
    elif value1 == "1":
        val1="0"
    if value2 == "0":
        val2="1"
    elif value2 == "1":
        val2="0"
    print(val1," ",val2)

l4 = Label(frame1, text="Alegeti operatia dorita", fg="maroon", font="Times")
l4.pack(pady=20)
frame2=Frame(height = 60,width = 1000,bg="red")
frame2.pack()
andbtn=Button(frame2, text="AND", bd=5,width=18, bg="indian red",command=AND_GATE).pack(side=LEFT, expand=NO, anchor=NE, pady=2)
orbtn = Button(frame2, text="OR", bd=5 ,width=18,bg="indian red",command=OR).pack(side=LEFT, expand=YES, anchor=NE, pady=2)
notbtn=Button(frame2, text="NOT", bd=5,width=18, bg="indian red",command=NOT).pack(side=LEFT, expand=YES, anchor=NE, pady=2)
xorbtn = Button(frame2, text="XOR",bd=5,width=18, bg="indian red",command=XOR).pack(side=LEFT, expand=YES, anchor=NW, pady=2)
nxorbtn = Button(frame2, text="NXOR",bd=5,width=18, bg="indian red",command=NXOR).pack(side=LEFT, expand=YES, anchor=NW, pady=2)
norbtn = Button(frame2, text="NOR",bd=5,width=18, bg="indian red",command=NOR).pack(side=LEFT, expand=YES, anchor=NW, pady=2)
nandbtn=Button(frame2, text="NAND", bd=5 ,width=18,bg="indian red",command=NAND).pack(side=LEFT, expand=YES, anchor=NW, pady=2)


frame3=Frame(height = 360,width = 1000,bg="blue").pack()
photoAnd = PhotoImage(file="AND-gate.png")
labelPhoto1 = Label(frame3, image=photoAnd)
labelPhoto1.pack(pady=50)
myWindow.geometry("1000x600+120+20")
myWindow.mainloop()